<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::create('notes', function($table)
            {
                $table->increments('id');
                $table->string('desc');
                $table->string('title', 128);
                $table->date('start_date');
                $table->date('end_date');
                $table->string('order','10');
                $table->string('done','1');
                $table->timestamps();
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notes');
	}

}
