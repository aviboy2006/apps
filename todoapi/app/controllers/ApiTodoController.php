<?php
 
class ApiTodoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$notes = Note::all();		 
		return $notes;
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function store()
	{   

		$note = Note::create(Request::all());
		return $note;
 	/*	$note = new Note;
            $data = file_get_contents("php://input");
			$objData = json_decode($data);
			$note->desc = $objData->desc;
            $note->title = $objData->title;
            $note->start_date = $objData->start_date;
            $note->end_date = $objData->end_date;
            $note->order = $objData->order;*/
           /* $note->desc = Input::get('desc');
            $note->title = Input::get('title');
            $note->start_date = Input::get('start_date');
            $note->end_date = Input::get('end_date');
            $note->order = Input::get('order');*/
 
           /* if($note->save()){
              return '{ "flag":"true", "msg": "Save successfully"}'; 
            }
            else{
               return '{ "flag":"false", "msg": "Error while creating"}'; 
            }*/
           // return json_encode($note);
             
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
 

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{ 
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{  

		$note = Note::find($id);
		$note->done = Request::input('done');
		$note->save();
 
		return $note;
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
 
	public function destroy($id) {		 
		Note::destroy($id);
	}


}
