<?php
Route::model('note', 'Note');
/*Route::get('/', array('as' => 'home', 'uses'=>'ApiTodoController@index'));
 
Route::post('/create', 'ApiTodoController@create');
Route::post('/update', 'ApiTodoController@update');
Route::get('/delete/{id}', 'ApiTodoController@delete');*/
Route::resource('api','ApiTodoController');
Route::get('/login', array('as' => 'login', 'uses'=>'AuthController@getLogin'))->before('guest');
Route::post('login', array('uses'=>'AuthController@postLogin'))->before('csrf');