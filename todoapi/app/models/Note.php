<?php
class Note extends Eloquent
{
	private $desc;
	private $title;
	private $start_date;
	private $end_date;
	private $order;
	private $done;
	protected $fillable = array('desc','title','end_date','start_date','order','done');
}