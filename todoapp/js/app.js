
 

var app = angular.module('MyNote', []);

app.directive("datepicker", function () {
  return {
    restrict: "A",
    require: "ngModel",
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText);
        });
      };
      var options = {
        dateFormat: "dd/mm/yy",
        onSelect: function (dateText) {
          updateModel(dateText);
        }
      };
      elem.datepicker(options);
    }
  }
});


app.controller('customersCtrl', function($scope, $http, $window) {

	$scope.notes = [];
	$scope.loading = false;
    /*$http.get(API_URL)
    .success(function(response) {$scope.notes = response;});*/

      $scope.custom = true;
        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;
      };
      
      $scope.init = function() { 
		$scope.loading = true;
		$http.get(API_URL).
		success(function(data, status, headers, config) { 
			$scope.notes = data;
		    $scope.loading = false;
 
		});
	 }

      $scope.addTodo = function() {
        $scope.loading = true;
 
		    $http.post(API_URL, { 
		    	title: $scope.task.title,
		    	desc: $scope.task.desc,
		        start_date: $scope.task.startdate,
		        end_date: $scope.task.enddate,
		        order: $scope.task.order,
		        done: 0
		      }).success(function(data, status, headers, config) {
		      $scope.notes.push(data);
		      $scope.note = '';
		        $scope.loading = false;
		        $window.location.href = APP_URL;
		 
		    });
 	  };
     
     $scope.updateTodo = function(notes) {
      $scope.loading = true;
   
        $http.put(API_URL+'/'+notes.id, {
          title: notes.title,
          done: notes.done
        }).success(function(data, status, headers, config) {
             note = data;
            $scope.loading = false;
     
        });
    };

     $scope.deleteTodo = function(index) { 
       $scope.loading = true;        
        var note = $scope.notes[index];
        $http.delete(API_URL+'/'+note.id)
        .success(function() {
          $scope.notes.splice(index, 1);
            $scope.loading = false;
   
        });
    };
 $scope.init();

});

