

<!DOCTYPE html>
<html lang="en-US">

<link rel="stylesheet" href="css/style.css">
 <?php include('header.php');?>
 <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
 
 <script src="js/app.js"></script>
<script type="text/javascript">
	
 
/*
var app = angular.module('myApp', []);
app.controller('customersCtrl', function($scope, $http) {
    $http.get("<?=API_URL?>")
    .success(function(response) {$scope.notes = response;console.log($scope.notes);});

    $scope.custom = true;
        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;
      };*/
     
     /*$scope.updateTodo = function(todo) {
      $scope.loading = true;
   
        $http.put('/api/todos/' + todo.id, {
          title: todo.title,
          done: todo.done
        }).success(function(data, status, headers, config) {
          todo = data;
            $scope.loading = false;
     
        });
    };*/

     /*$scope.deleteTodo = function(todo) { 
       $scope.loading = true;        
        
        $http.delete('<?=API_URL?>/' + todo.id)
        .success(function() {
          $scope.todos.splice(index, 1);
            $scope.loading = false;
   
        });
    };*/
 //$scope.init();
//});
</script>
<style type="text/css">
/*	.item{height: auto;border:2px solid #eee;}
	.item { width: 25%; }
.item.w2 { width: 50%; }*/
</style>
<body>
 
<!-- <h1> List of task <small><a href="create_task.php"> Create New Task</a></small></h1>
<div ng-app="myApp" ng-controller="customersCtrl" class="js-masonry" data-masonry-options='{ "itemSelector": ".item", "columnWidth": 200 }'>
  <div class="item " ng-repeat="x in notes"> 
     <h1>{{ x.title }} </h1>
     <p> {{ x.desc }} </p>
     
  </div>
   
</div>
 -->

<div class="container" ng-app="MyNote" ng-controller="customersCtrl">
  <section class="todo">
    <ul class="todo-controls">
      <li><a href="<?=APP_URL?>create_task.php" class="icon-add">Add</a></li>
      
    </ul>
  
    <ul class="todo-list" >
      <li class="done" ng-repeat="note in notes">
        <input type="checkbox" ng-true-value="1" ng-false-value="'0'" ng-model="note.done" ng-change="updateTodo(note)">
        
         <span ng-click="toggleCustom()" class="listlabel {{ note.order }}">{{ note.title }}</span>
         <a href="javascript:void(0);" class="icon-delete" ng-click="deleteTodo($index)">Delete</a>
        <p ng-hide="custom">{{ note.desc }}</p>
      </li>
      <!-- <li>
        <input type="checkbox" id="build"/> 
        <label class="toggle" for="build"></label>
        Build it!
      </li>
      <li>
        <input type="checkbox" id="ship"/> 
        <label class="toggle" for="ship"></label>
        Ship it...<br />
        with a line break!
      </li> -->
    </ul>
    
    <ul class="todo-pagination">
      <li class="previous"><span><i class="icon-previous"></i> Previous</span></li>
      <li class="next"><a href="javascript:void(0);">Next <i class="icon-next"></i></a></li>
    </ul>
  </section>
</div>
</body>
</html>